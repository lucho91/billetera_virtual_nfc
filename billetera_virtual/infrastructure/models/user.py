import uuid

from django.contrib.auth.models import AbstractUser
from .base.base_model import BaseModel
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.validators import RegexValidator

phone_validator = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message=_("Enter a valid phone number. This value may contain only numbers."))


class User(AbstractUser, BaseModel):
    id = models.UUIDField(
        primary_key=True, 
        default=uuid.uuid4, 
        editable=False
    )
    first_name = models.CharField(
        _("first name"), 
        max_length=150, 
        blank=True, 
        null=True
    )
    last_name = models.CharField(
        _("last name"), 
        max_length=150, 
        blank=True, 
        null=True
    )
    full_name = models.CharField(
        _("full name"), 
        max_length=400, 
        blank=False, 
        null=False
    )
    identification = models.CharField(
        max_length=100,
        blank=False, 
        null=True, 
        unique=True,  
        db_column='identification',
        error_messages={
            "unique": _("main.users.validations.already_identification_user"),
        }
    )
    username_validator = UnicodeUsernameValidator()
    username = models.CharField(
        _("username"),
        max_length=50,
        unique=True,
        help_text=_(
            "Required. 50 characters or fewer. Letters, digits and @/./+/-/_ only."
        ),
        validators=[username_validator],
        error_messages={
            "unique": _("main.users.validations.already_username_user"),
        },
    )
    phone = models.CharField(
        _("phone"),
        max_length=20,
        unique=True,
        validators=[phone_validator],  # Usar el validador personalizado
        error_messages={
            "unique": _("main.users.validations.already_phone_user"),
        },
    )
    email = models.EmailField(
        blank=True, 
        null=True, 
        db_column='email',
    )
    is_active = models.BooleanField(
        default=True,
        db_column='is_active'
    )
    class Meta(AbstractUser.Meta):
      ordering = ['id']
      swappable = "AUTH_USER_MODEL"
      db_table = "user"

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = [""]
