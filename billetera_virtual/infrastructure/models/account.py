from django.db import models
from .base.base_model import BaseModel
from .user import User

class Account(BaseModel):
  current_balance = models.FloatField(
    db_column="value"
  )
  user = models.ForeignKey(
      User,
      on_delete=models.CASCADE,
      related_name="user_account",
      db_column="user_id",
  )
  last_balacen_update = models.DateTimeField(
    verbose_name="Fecha de actualización balance",
    db_column="last_balacen_update",
  )
  is_active = models.BooleanField(
    default=True,
    db_column='is_active'
  )

  class Meta: 
    db_table = "account"
    ordering =['id']