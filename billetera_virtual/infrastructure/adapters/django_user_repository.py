from typing import List
from billetera_virtual.infrastructure.models.user import (
    User,
)
from billetera_virtual.application.ports.repositories.user_repository import UserRepositoryInterface
from django.db import transaction
from django.utils import timezone



class DjangoUserRepository(UserRepositoryInterface):
    
    def create_user(self, data):
        with transaction.atomic():
            user = User.objects.create(**data)
            user = User.objects.get(id=user.id)
            user.set_password(data["password"])
            user.created_by  = data["created_by"]
            user.save()
            return user

    def update_user(self, data, user):
        id_user = data.pop("id")
        data["modified_in"] = timezone.now()
        data["modified_by"] = user.id
        password = data.pop("password", None)
        user = User.objects.filter(id=id_user).update(**data)
        if password:
            user = User.objects.filter(id=id_user).first()
            user.set_password(password)
            user.save()
        return user

    def list_users(self):
        return (
            User.objects.select_related(
                "rol_id", "city", "city__state_id", "city__state_id__country_id"
            )
            .filter()
        )

    def get_user_by_id(self, pk):
        return User.objects.filter(id=pk).first()