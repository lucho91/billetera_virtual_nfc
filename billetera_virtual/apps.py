from django.apps import AppConfig


class BilleteraVirtualConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'billetera_virtual'
