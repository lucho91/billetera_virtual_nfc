from rest_framework import generics
from rest_framework.response import Response
from billetera_virtual.application.ports.serializers.my_token_obtain_pair_serializer import MyTokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView

import json

class LoginUserView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        return Response(data)