# Generated by Django 4.2.6 on 2024-02-12 01:54

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('billetera_virtual', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='TypeMovements',
            fields=[
                ('deleted', models.DateTimeField(db_index=True, editable=False, null=True)),
                ('deleted_by_cascade', models.BooleanField(default=False, editable=False)),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created_in', models.DateTimeField(auto_now_add=True, db_column='created_in', null=True, verbose_name='Fecha de creación')),
                ('modified_in', models.DateTimeField(auto_now=True, db_column='modified_in', null=True, verbose_name='Fecha de actualización')),
                ('created_by', models.UUIDField(db_column='created_by', editable=False, null=True)),
                ('modified_by', models.UUIDField(db_column='modified_by', editable=False, null=True)),
                ('deleted_by', models.UUIDField(db_column='deleted_by', editable=False, null=True)),
                ('name', models.CharField(max_length=150, verbose_name='name')),
                ('code', models.CharField(db_column='code', max_length=100, unique=True)),
            ],
            options={
                'db_table': 'type_movements',
                'ordering': ['id'],
            },
        ),
    ]
