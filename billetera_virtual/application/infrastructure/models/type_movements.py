from django.db import models
from .base.base_model import BaseModel




class TypeMovements(BaseModel):
  name        = models.CharField(max_length=150,verbose_name='name')
  code = models.CharField(max_length=100,blank=False,db_column='code',unique=True)
  
  class Meta: 
    db_table = "type_movements"
    ordering =['id']