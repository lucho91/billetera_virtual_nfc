import uuid
from django.db import models
from safedelete.models import SafeDeleteModel, SOFT_DELETE_CASCADE
from safedelete.models import SOFT_DELETE
from .manager import BasicManager
from django.utils import timezone


# Create your models here.
class BaseModel(SafeDeleteModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    _safedelete_policy = SOFT_DELETE_CASCADE
    created_in = models.DateTimeField(
        auto_now_add=True,
        blank=True,
        null=True,
        verbose_name="Fecha de creación",
        db_column="created_in",
    )
    modified_in = models.DateTimeField(
        auto_now=True,
        verbose_name="Fecha de actualización",
        null=True,
        db_column="modified_in",
    )
    created_by = models.UUIDField(null=True, editable=False, db_column="created_by")
    modified_by = models.UUIDField(null=True, editable=False, db_column="modified_by")
    deleted_by = models.UUIDField(null=True, editable=False, db_column="deleted_by")

    objects = BasicManager()

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        user = kwargs.pop("user", None)
        self.modified_in = timezone.now()
        if user:
            self.modified_by = user.id
            if not self.created_by:
                self.created_by = user.id
        super().save(*args, **kwargs)
