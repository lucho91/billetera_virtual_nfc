from rest_framework_simplejwt.serializers import TokenObtainPairSerializer, TokenRefreshSerializer
from django.contrib.auth import authenticate
from django.contrib.auth.models import update_last_login
from rest_framework import exceptions
from rest_framework_simplejwt.serializers import api_settings


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
  """
  Serializer para la customizar los parámetros encriptados en el JWT. 
  """
  
  def validate(self, attrs):
    authenticate_kwargs = {
        self.username_field: attrs[self.username_field],
        "password": attrs["password"],
    }
    try:
        authenticate_kwargs["request"] = self.context["request"]
    except KeyError:
        pass

    self.user = authenticate(**authenticate_kwargs)

    if not api_settings.USER_AUTHENTICATION_RULE(self.user):
        raise exceptions.AuthenticationFailed(
            "No se encontró ninguna cuenta activa con las credenciales proporcionadas",
            "no_active_account",
        )

    data = {}
    refresh = self.get_token(self.user)

    data["refresh"] = str(refresh)
    data["access"] = str(refresh.access_token)

    if api_settings.UPDATE_LAST_LOGIN:
        update_last_login(None, self.user)
    return data

  @classmethod
  def get_token(cls, user):
    token = super().get_token(user)
    if user.last_login:
        token['last_login'] = user.last_login.strftime("%Y/%m/%d - %H:%M:%S")
    # user.last_login = datetime.now()
    # user.save()
    token['user'] = user.email
    return token