from rest_framework.fields import SerializerMethodField, CharField, IntegerField, BooleanField, UUIDField, DecimalField
from rest_framework.serializers import ModelSerializer


class BaseSerializer(ModelSerializer):

    class Meta:
        abstract = True
        fields = ()

    def __init__(self, *args, **kwargs):
        """
        Este método configura el polimorfismo del constructor de la clase.
        Aún así se ejecuta el constructor en su estado natural.
        :param args: Argumentos
        :param kwargs: Argumentos adicionales
        """

        # Se eliminan los argumentos adicionales parametrizados para evitar
        # que se envíen al constructor de la superclase
        fields = kwargs.pop('fields', None)
        exclude = kwargs.pop('exclude', None)
        include = kwargs.pop('include', None)
        default_excluded = self.get_extra_kwargs().get("default_excluded", None)

        # Instancia del constructor de la superclase.
        super(BaseSerializer, self).__init__(*args, **kwargs)
        self.update_field_error_messages(self.fields)

        if default_excluded is not None:
            """
            Si se especifican campos excluidos por defecto, se omitirán de la serialización 
            """
            for field_name in default_excluded:
                self.fields.pop(field_name)

        if fields is not None:
            """
            Si se especifican las fields, se eliminan los campos que no se contemplen
            """
            allowed = set(fields)
            existing = set(self.fields)
            for field_name in existing - allowed:
                self.fields.pop(field_name)

        if exclude is not None:
            """
            Si se especifican campos en exclude, se omitirán de la serialización 
            """
            for field_name in exclude:
                self.fields.pop(field_name)

        if include is not None:
            """
            Si se especifican campos en include, se agregarán en la serialización 
            """
            for field_to_include in include:
                self.fields[field_to_include["label"]] = self.switch_configuration(field_to_include["configuration"])

    default_error_messages = {
        'blank': 'errors.thisFieldCannotBlank',
        'required': 'Campos Requeridos',
    }

    def update_field_error_messages(self, fields):
        for field_name, field in fields.items():
            field.error_messages.update(self.default_error_messages)

    def switch_configuration(self, configuration):
        switcher = {
            "method": SerializerMethodField(),
            "char": CharField(),
            "integer": IntegerField(),
            "boolean": BooleanField(),
            "uuid": UUIDField(),
            "decimal": DecimalField(max_digits=20, decimal_places=2)
        }
        return switcher.get(configuration, configuration)
    
    def cut_string(self, string_incoming):
        if len(string_incoming) > 16:
            return string_incoming[0:15] + "..."
        else:
            return string_incoming

    def cut_string_email(self, string_incoming):
        if len(string_incoming) > 21:
            return string_incoming[0:20] + "..."
        else:
            return string_incoming