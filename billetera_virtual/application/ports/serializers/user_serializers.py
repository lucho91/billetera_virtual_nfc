from billetera_virtual.application.ports.serializers.base_serlializer import BaseSerializer
from billetera_virtual.infrastructure.models.user import User
from rest_framework import serializers


class UserSerializerList(BaseSerializer):

    def to_representation(self, instance):
        country = None
        state = None
        city = None
        representation = super().to_representation(instance)
        representation["country"] = None
        representation["state"] = None
        representation["city"] = None
        representation["country_name"] = None
        representation["state_name"] = None
        representation["city_name"] = None
        representation["rol_name"] = None
        representation["location"] = None

        try:
            representation["date_joined_formatted_date_es"] = instance.date_joined_formatted_date_es
            representation["date_joined_formatted_date_en"] = instance.date_joined_formatted_date_en
            representation["date_joined_formatted_dmy"] = instance.date_joined_formatted_dmy
            representation["location"] = instance.location
        except:
            pass

        if instance.city:
            city = instance.city
            state = city.state_id
            country = state.country_id
            representation["country"] = country.id
            representation["state"] = state.id
            representation["city"] = city.id
            representation["country_name"] = country.name
            representation["state_name"] = state.name
            representation["city_name"] = city.name
        if instance.rol_id:
            representation["rol_name"] = instance.rol_id.name
        return representation

    full_name = serializers.CharField(
        read_only=True,
    )
    country_department_city = serializers.SerializerMethodField()
    def get_country_department_city(self, instance):
        if instance.city:
            city = instance.city
            state = city.state_id
            country_department_city = f"{state.name} - {city.name}"
            return country_department_city
        return None
    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'rol_id', 
            'identification_type_id', 
            # 'user_id',
            'identification',
            'email', 
            'date_joined',
            'picture',
            'phone',
            'phone_2',
            'user_status',
            'full_name',
            'first_name',
            'last_name',
            'in_association',
            'association_name',
            'is_superuser',
            'created_by',
            'created_in',
            'modified_by',
            'modified_in',
            'country_department_city',
            'is_active',
            'address',
        )

class UserSerializerList2(UserSerializerList):
    def to_representation(self, instance):
        q = User.objects.filter(id=instance.created_by)
        creado = None
        if q.exists():
            creado = q.first().first_name
        qq = User.objects.filter(id=instance.modified_by)
        editado = None
        if qq.exists():
            editado = qq.first().first_name
        representation = super().to_representation(instance)
        representation["created_by"] = creado
        representation["modified_by"] = editado
        return representation
    
    class Meta:
        model = User
        fields = "__all__"
        
class UserSerializerCreate(BaseSerializer):
    created_by = serializers.UUIDField()
    
    class Meta:
        model = User
        fields = (
            'rol_id', 
            'identification_type_id',
            'identification',
            'email',
            'phone',
            'phone_2',
            'first_name',
            'last_name',
            'user_status',
            'city',
            'created_by',
            'address',
        )

class AnswerSerializer(serializers.Serializer):
    question_id = serializers.UUIDField(format='hex_verbose')
    answer = serializers.CharField()
class ActivatedUserSerializer(serializers.Serializer):
    password = serializers.CharField(allow_null=True, required=False)
    in_association = serializers.BooleanField(allow_null=True, required=False)
    association_name = serializers.CharField(allow_null=True, required=False)
    answers = AnswerSerializer(many=True)