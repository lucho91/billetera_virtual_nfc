from abc import ABC, abstractmethod
from typing import List
from django.db.models import QuerySet

class UserRepositoryInterface(ABC):

    @abstractmethod
    def create_user(self, data):
        """Create a new user with the given username, email, and password."""
        pass

    @abstractmethod
    def update_user(self, data) -> int:
        pass
    
    @abstractmethod
    def list_users(self) -> QuerySet:
        """list all users"""
        pass

    @abstractmethod
    def get_user_by_id(self, pk):
        """find user by id"""
        pass